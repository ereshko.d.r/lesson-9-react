import React from 'react'
import classes from './MainWindow.module.css'
import FirstHeader from '../../textheaders/first-header/FirstHeader'

const MainWindow = ({header, headerItems, children, ...props}) => {
  return (
    <div className={classes.MainWindow}>
        <div className={classes.WindowHeader}>
            <FirstHeader>{header}</FirstHeader>
            {headerItems}
        </div>
        <div className={classes.WindowBody}>
            {children}
        </div>
    </div>
  )
}

export default MainWindow