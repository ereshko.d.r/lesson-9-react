import React from 'react'
import classes from './Tab.module.css'
import ThirdHeader from '../../textheaders/third-header/ThirdHeader'

const Tab = ({path, currentPage, onClick, children, ...props}) => {

    return (
        <div 
            className={
                [classes.Tab, (path === currentPage) ? classes.Active : ''].join(' ')
            }
            onClick={() => onClick(path)}
        >
            <ThirdHeader>{children}</ThirdHeader>
        </div>
    )
}

export default Tab