import React from 'react'
import Icon from '../../../../images/UI/send-button.svg'
import classes from './SenderButton.module.css'

const SenderButton = ({...props}) => {
  return (
    <button {...props} className={classes.SenderButton}>
        <img src={Icon} alt="" />
    </button>
  )
}

export default SenderButton