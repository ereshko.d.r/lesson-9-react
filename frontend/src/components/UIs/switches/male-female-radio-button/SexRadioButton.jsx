import React from 'react'
import Female from '../../../images/UI/female.svg'
import Male from '../../../images/UI/male.svg'
import classes from './SexRadioButton.module.css'

const SexRadioButton = ({value, onChange, ...props}) => {
    
    return (
        <div className={classes.SexRadioButtonContainer}>
                <div className={classes.SexRadioButton}>
                    <input type="radio" name="sex" id="female" onChange={event => onChange(event)}/>
                    <label htmlFor="female"><img src={Female} alt="" /></label>
                </div>
                
                <div className={classes.SexRadioButton}>
                    <input type="radio" name="sex" id="male" onChange={event => onChange(event)}/>
                    <label htmlFor="male"><img src={Male} alt="" /></label>
                </div>
            </div>
    )
}

export default SexRadioButton