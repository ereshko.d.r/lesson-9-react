import React from 'react'
import classes from './SwitchToggle.module.css'

const SwitchToggle = ({nameSwitch, value, onChange, ...props}) => {
    return (
        <div className={classes.SwitchToggleContainer}>
            <label className={classes.SwitchToggle}>
                <input type="checkbox" name={nameSwitch} id="" value={value} onChange={onChange}/>
                <span className={classes.Slider}></span>
            </label>
        </div>
    )
}

export default SwitchToggle