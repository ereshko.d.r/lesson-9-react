import React, { useState } from 'react'
import Cross from '../../images/symbols/large-cross.svg'
import Zero from '../../images/symbols/large-zero.svg'
import classes from './Cell.module.css'

const Cell = ({game, move, id, restart, setShowModal, ...props}) => {
    const [symbol, setSymbol] = useState('')
    const [winner, setWinner] = useState('#FFFFFF')
    const [cursor, setCursor] = useState('pointer')

    if (!game.isGameStarted() && symbol) {
        setSymbol('')
        setWinner('#FFFFFF')
        setCursor('pointer')
    }

    if (game.combination) {
        if (game.isGameOver && winner === '#FFFFFF' && game.combination.includes(id)) {
            switch (game.winner.symbol) {
                case 'x':
                    setWinner('#CFEDE6')
                    break;
                case 'o':
                    setWinner('#F3BBD0')
                    break;
                default:
                    break;
            }
        }
    }
    
    const putSymbol = (id, event) => {
        if (game.isGameOver) {
            event.preventDefault()
            return setShowModal(true)
        }
        const currentSymbol = game.currentPlayer.symbol
        try {
            if (symbol === '') {
                move(id)
                setCursor('default')
                switch (currentSymbol) {
                    case 'x':
                        setSymbol(Cross)
                        break;
                    case 'o':
                        setSymbol(Zero)
                        break;
                    default:
                        break;
                }
            }
        }
        catch (error) {

        }        
    }

    return (
        <div
            className={classes.Cell}
            id={id}
            onClick={event => putSymbol(id, event)}
            style={{cursor: cursor, background: winner}}
        >
            {(symbol) ? <img src={symbol} alt="" /> : <img src='' alt=''/>}
        </div>
    )
}

export default Cell