import React, { useState } from 'react'
import {Outlet, useNavigate, useLocation} from 'react-router-dom'
import classes from './Header.module.css'
import Logo from '../../images/UI/small-logo.svg'
import LogoutButton from '../../UIs/buttons/normal/logout-button/LogoutButton'
import Tab from '../../UIs/tabs/tab/Tab'

const Header = () => {
    const navigate = useNavigate()
    const location = useLocation()
    const [currentPage, setCurrentPage] = useState(location.pathname)

    const fakeLogout = () => {
        navigate('/auth')
    }

    const redirect = (path) => {
        setCurrentPage(path)
        navigate(path)
    }

    return (
        <>
        <header className={classes.Header}>
            <div><img src={Logo} alt="" /></div>
            <nav className={classes.Navbar}>
                <Tab path={'/game'} currentPage={currentPage} onClick={redirect}>Игровое поле</Tab>
                <Tab path={'/rating'} currentPage={currentPage} onClick={redirect}>Рейтинг</Tab>
                <Tab path={'/active'} currentPage={currentPage} onClick={redirect}>Активные игроки</Tab>
                <Tab path={'/history'} currentPage={currentPage} onClick={redirect}>История игр</Tab>
                <Tab path={'users'} currentPage={currentPage} onClick={redirect}>Список игроков</Tab>
            </nav>
            <LogoutButton onClick={fakeLogout}/>
        </header>
        <Outlet />
        </>
    )
}

export default Header