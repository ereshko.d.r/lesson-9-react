import React, { useEffect } from 'react'
import classes from '../style/AuthPage.module.css'
import AuthForm from '../modules/AuthForm'

const AuthPage = () => {
    const setTitle = useEffect(() => {
        document.title = 'Авторизация'
    }, [])
    return (
        <div className={classes.Main}>
            {setTitle}
            <AuthForm />
        </div>
    )
}

export default AuthPage