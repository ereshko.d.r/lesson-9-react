import React, { useState } from 'react'
import Male from '../../../../images/UI/male.svg'
import Female from '../../../../images/UI/female.svg'
import NormalStatusBar from '../../../../UIs/statusbars/normal-statusbar/NormalStatusBar'
import BlockButton from '../../../../UIs/buttons/normal/block-button/BlockButton'
import classes from './UsersRow.module.css'

const UsersRow = ({user, ...props}) => {
    const [blockStatus, setBlockStatus] = useState(user.blockStatus)

    const getFullName = () => {
        return `${user.firstName} ${user.secondName} ${user.surName}`
    }

    const getSexIcon = () => {
        if (user.sex === 'male') {
            return <img src={Male} alt='' width='24px'/>
        }
        return <img src={Female} alt='' width='24px'/>
    }

    const getStatusBar = (status) => {
        if (status) {
            return <NormalStatusBar color={'red'}>Заблокирован</NormalStatusBar>
        }
        return <NormalStatusBar color={'green'}>Активен</NormalStatusBar>
    }

    const getTime = (time) => {
        const options = { day: 'numeric', month: 'long', year: 'numeric'}
        const date = new Date(Number(time) * 1000).toLocaleString('ru-RU', options)
        const result = date.split(' ')
        return `${result[0]} ${result[1]} ${result[2]}`
    }

    const changeStatus = () => {
        if (blockStatus) {
            setBlockStatus(false)
        }
        else {
            setBlockStatus(true)
        }
    }

    return (
        <div className={classes.UsersRow}>
            <div>{getFullName()}</div>
            <div>{user.age}</div>
            <div>{getSexIcon()}</div>
            <div className={classes.NormalStatusBar}>{getStatusBar(blockStatus)}</div>
            <div>{getTime(user.created)}</div>
            <div>{getTime(user.lastChange)}</div>
            <div className={classes.BlockButtons}>
                {(blockStatus) ?
                <BlockButton action={changeStatus}>Разблокировать</BlockButton> :
                <BlockButton action={changeStatus} block>Заблокировать</BlockButton>}
            </div>
        </div>
    )
}

export default UsersRow