import React, { useState } from 'react'
import Icon from '../../../../images/UI/exit-cross.svg'
import classes from './UserModalContent.module.css'
import SexRadioButton from '../../../../UIs/switches/male-female-radio-button/SexRadioButton'
import FirstHeader from '../../../../UIs/textheaders/first-header/FirstHeader'
import ThirdHeader from '../../../../UIs/textheaders/third-header/ThirdHeader'
import Input from '../../../../UIs/inputs/Input'
import SubmitButton from '../../../../UIs/buttons/normal/submit-button/SubmitButton'

const UserModalContent = ({setShowModal, users, addUser}) => {
    const [name, setName] = useState('')
    const [age, setAge] = useState('')
    const [sex, setSex] = useState('')
    const disabled = (name.split(' ').length === 3 && age && sex) ? false : true

    const changeInput = (event) => {
        switch (event.target.name) {
            case 'name':
                setName(event.target.value.replace(/([\d])/g, ''))
                break;
            case 'age':
                setAge(event.target.value.replace(/[^\d]/g, ''))
                break;
            case 'sex':
                setSex(event.target.id)
                break;
            default:
                break;
        }
    }

    const submitData = () => {
        const splitedName = name.split(' ')
        const created = new Date().getTime()
        console.log(created)
        const newUserData = {
            id: Number(users.at(-1).id) + 1,
            firstName: splitedName[0],
            secondName: splitedName[1],
            surName: splitedName[2],
            age: age,
            sex: sex,
            created: Number(created / 1000),
            lastChange: Number(created / 1000),
            blockStatus: false,
            gameStatus: "active",
            totalGames: "0",
            wins: "0",
            looses: "0",
            winRate: "0"
        }
        addUser([...users, newUserData])
        setAge('')
        setName('')
        setSex('')
        setShowModal(false)
    }

    return (
        <div className={classes.UserModalContent}>
            <div className={classes.CrossExitContainer}>
                <div className={classes.CrossExit}>
            </div>
                <img src={Icon} alt="" onClick={() => setShowModal(false)}/>
            </div>
            <FirstHeader>Добавьте игрока</FirstHeader>
            <div className={classes.SubjectName}>
                <ThirdHeader>ФИО</ThirdHeader>
                <Input placeholder={'Фамилия Имя Отчество'} name={'name'} onChange={changeInput} value={name}/>
            </div>
            <div className={classes.SubjectProperty}>
                <div className={classes.SubjectAge}>
                    <ThirdHeader>Возраст</ThirdHeader>
                    <Input placeholder={0} name={'age'} onChange={changeInput} value={age}/>
                </div>
                <div className={classes.SubjectSex}>
                    <ThirdHeader>Пол</ThirdHeader>
                    <SexRadioButton onChange={changeInput}/>
                </div>
            </div>
            <SubmitButton disabled={disabled} onClick={submitData}>Добавить</SubmitButton>
        </div>
    )
}

export default UserModalContent