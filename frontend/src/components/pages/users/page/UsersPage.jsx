import React, { useState, useEffect } from 'react'
import classes from '../style/UsersPage.module.css'
import MainWindow from '../../../UIs/windows/main-window/MainWindow'
import SubmitButton from '../../../UIs/buttons/normal/submit-button/SubmitButton'
import UsersContent from '../modules/users-content/UsersContent'
import UsersRow from '../modules/users-row/UsersRow'
import ModalWindow from '../../../UIs/modals/ModalWindow'
import UserModalContent from '../modules/modal-content/UserModalContent'

const UsersPage = ({users, setUserList, ...props}) => {
    const WINDOW_HEADER = 'Список игроков'

    const setTitle = useEffect(() => {
        document.title = WINDOW_HEADER
    }, [])

    const [showModal, setShowModal] = useState(false)

    const addUserButton = <SubmitButton onClick={() => setShowModal(true)}>
        Добавить игрока
    </SubmitButton>    

    return (
        <div className={classes.Main}>
            {setTitle}
            <ModalWindow showModal={showModal} setShowModal={setShowModal}>
                <UserModalContent setShowModal={setShowModal} users={users} addUser={setUserList}/>
            </ModalWindow>
            <div className={classes.Content}>
                <MainWindow header={WINDOW_HEADER} headerItems={addUserButton}>
                    <UsersContent>
                        {users.map((user, index) => (
                            <UsersRow user={user} key={index} />
                        ))}
                    </UsersContent>
                </MainWindow>
            </div>
        </div>
    )
}

export default UsersPage