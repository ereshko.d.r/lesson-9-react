import React from 'react'
import classes from './GameFieldSubjectRow.module.css'
import Cross from '../../../../../images/symbols/cross.svg'
import Zero from '../../../../../images/symbols/zero.svg'
import ThirdHeader from '../../../../../UIs/textheaders/third-header/ThirdHeader'

const GameFieldSubjectRow = ({user, ...props}) => {
    const Icon = (user.symbol === 'x') ? Cross : Zero
    const fullName = `${user.firstName} ${user.secondName} ${user.surName}`

    return (
        <div className={classes.GameFieldSubjectRow}>
            <img src={Icon} alt="" />
            <div className={classes.GameFieldSubjectInfo}>
                <div className={classes.SubjectName}><ThirdHeader>{fullName}</ThirdHeader></div>
                <div className={classes.SubjectWinRate}>{user.winRate}% побед</div>
            </div>
        </div>
    )
}

export default GameFieldSubjectRow