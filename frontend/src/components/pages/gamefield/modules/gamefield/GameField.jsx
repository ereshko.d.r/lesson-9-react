import React from 'react'
import classes from './GameField.module.css'
import Cell from '../../../../UIs/cells/Cell'
import TicTacToeLogic from '../../../../scripts/game_logic'
import Cross from '../../../../images/symbols/cross.svg'
import Zero from '../../../../images/symbols/zero.svg'
import ModalWindow from '../../../../UIs/modals/ModalWindow'
import ModalContent from '../modal-content/ModalContent'

class GameField extends React.Component {
    constructor(props) {
        super(props)
        const player1 = props.users.player1
        const player2 = props.users.player2
        this.gameLogic = new TicTacToeLogic(player1, player2)
        this.state = {
            game: this.gameLogic,
            time: 0,
            timer: false,
            showModal: false
        }
        this.move = this.move.bind(this)
        this.restart = this.restart.bind(this)
        this.setShowModal = this.setShowModal.bind(this)
        this.countdownID = setInterval(
            () => this.tick(), 1000
        )
    }

    #startTimer() {
        this.setState({timer: true})
    }

    #stopTimer() {
        this.setState({timer: false})
    }

    tick() {
        if (this.state.timer) {
            this.setState({time: this.state.time + 1})
        }
    }

    timer(time) {
        const seconds = (time % 60 < 10) ? `0${time % 60}` : time % 60
        const minute = (Math.floor(time / 60) < 10) ? `0${Math.floor(time / 60)}` : Math.floor(time / 60)
        return `${minute}:${seconds}`
    }

    move(numOfField) {
        const resultOfMove = this.gameLogic.game(numOfField)
        console.log(resultOfMove)
        if (!resultOfMove) {
            this.#startTimer()
        }
        else {
            this.#stopTimer()
            this.setShowModal(true)

        }
        this.setState({game: this.gameLogic})
    }

    componentWillUnmount() {
        clearInterval(this.countdownID)
    }

    getCurrentSymbol() {
        if (this.gameLogic.currentPlayer.symbol === 'x') {
            return <img src={Cross} alt="" />
        }
        return <img src={Zero} alt="" />
    }

    restart() {
        this.gameLogic.restart()
        this.setState({
            game: this.gameLogic,
            time: 0,
            timer: false,
            showModal: false,
        })
        
    }

    setShowModal(bool) {
        this.setState({showModal: bool})
    }

    render() {
        return (
            <div className={classes.GameFieldContainer}>
                <div className={classes.GameFieldTime}>{this.timer(this.state.time)}</div>
                <div className={classes.GameFieldBoard}>
                    <Cell move={this.move} game={this.state.game} id={0} setShowModal={this.setShowModal}/>
                    <Cell move={this.move} game={this.state.game} id={1} setShowModal={this.setShowModal}/>
                    <Cell move={this.move} game={this.state.game} id={2} setShowModal={this.setShowModal}/>
                    <Cell move={this.move} game={this.state.game} id={3} setShowModal={this.setShowModal}/>
                    <Cell move={this.move} game={this.state.game} id={4} setShowModal={this.setShowModal}/>
                    <Cell move={this.move} game={this.state.game} id={5} setShowModal={this.setShowModal}/>
                    <Cell move={this.move} game={this.state.game} id={6} setShowModal={this.setShowModal}/>
                    <Cell move={this.move} game={this.state.game} id={7} setShowModal={this.setShowModal}/>
                    <Cell move={this.move} game={this.state.game} id={8} setShowModal={this.setShowModal}/>
                </div>
            <div className={classes.GameFieldStep}>Ходит&nbsp;&nbsp;{this.getCurrentSymbol()}&nbsp;&nbsp;{this.state.game.currentPlayer.getName}</div>
            <ModalWindow showModal={this.state.showModal} setShowModal={this.setShowModal}>
                <ModalContent winner={this.state.game.winner} restart={this.restart} setShowModal={this.setShowModal}/>
            </ModalWindow>
        </div>
        )
    }
}

export default GameField