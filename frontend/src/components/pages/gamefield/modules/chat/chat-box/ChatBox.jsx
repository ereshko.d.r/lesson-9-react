import React from 'react'
import classes from './ChatBox.module.css'

const ChatBox = ({user, time, text, ...props}) => {

    return (
        <div className={
            [classes.ChatBox, (user.symbol === 'x') ?
            classes.Cross : classes.Zero].join(' ')
        }>
            <div {...props} className={classes.ChatBoxHeader}>
                <div className={classes.UserName}>
                    {`${user.firstName} ${user.secondName}`}
                </div>
                <div className={classes.Time}>
                    {time}
                </div>
            </div>
            <div className={classes.ChatBoxContent}>
                {text}
            </div>
        </div>
    )
}

export default ChatBox