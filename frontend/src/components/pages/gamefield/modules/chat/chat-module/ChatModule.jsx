import React, {useEffect, useRef, useState } from 'react'
import classes from './ChatModule.module.css'
import ChatBox from '../chat-box/ChatBox'
import ChatInput from '../chat-input/ChatInput'

const ChatModule = ({users, messages, ...props}) => {
    const [messageList, setMessages] = useState([...messages])
    const [chatInput, setChatInput] = useState('')
    const [player, setPlayer] = useState(users.player1)

    const bottomRef = useRef(null)
    
    const changeMessage = (event) => {
        setChatInput(event.target.value)
    }

    const getAnotherPlayer = () => {
        if (player.symbol === 'x') {
            setPlayer(users.player2)
            return player
        }
        else {
            setPlayer(users.player1)
            return player
        }
    }

    const sendMessage = (event) => {
        event.preventDefault()
        const currentTime = new Date().toTimeString().split(':')
        if (chatInput) {
            const newMessage = {
                user: getAnotherPlayer(),
                time: `${currentTime[0]}:${currentTime[1]}`,
                text: chatInput,
            }
            setMessages([...messageList, newMessage])
            setChatInput('')
        }
    }

    useEffect(() => {
        bottomRef.current?.scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' });
    }, [messageList])

    return (
        <div className={classes.ChatModule}>
            <div className={classes.MessageHiderTop}></div>
            
            <div className={classes.ChatContent}>
                {(messageList.length !== 0) ? messageList.map((message, index) => (
                    <ChatBox {...message} key={index}/>
                )) : <div className={classes.EmptyBox}>
                        Сообщений еще нет
                    </div>}
                <div ref={bottomRef}></div> {/* Якорь для скроллинга к последнему сообщению */}
            </div>
            
            <div className={classes.ChatInput}>
            <div className={classes.MessageHiderBottom}></div>
                <ChatInput value={chatInput} onChange={changeMessage} submit={sendMessage}/>
            </div>
        </div>
    )
}

export default ChatModule