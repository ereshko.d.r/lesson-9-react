import React, { useEffect } from 'react'
import classes from '../style/GameFieldPage.module.css'
import GameFieldSubjects from '../modules/subjects/subjects-content/GameFieldSubjects'
import GameField from '../modules/gamefield/GameField'
import ChatModule from '../modules/chat/chat-module/ChatModule'


const GameFieldPage = ({users, ...props}) => {
    const setTitle = useEffect(() => {
        document.title = 'Игровое поле'
    }, [])
    return (
        <div className={classes.Main}>
            {setTitle}
            <div className={classes.GameFieldPageContent}>
                <div className={classes.GameSubjects}>
                    <GameFieldSubjects users={users} />
                </div>
                    <GameField users={users} />
                <div className={classes.ChatModule}>
                    <ChatModule users={users} messages={[]}/>
                </div>
            </div>
        </div>
    )
}

export default GameFieldPage