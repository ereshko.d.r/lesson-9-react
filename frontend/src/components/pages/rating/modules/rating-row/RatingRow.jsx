import React from 'react'
import classes from './RatingRow.module.css'

const RatingRow = ({user, ...props}) => {
    const getFullName = () => {
        return `${user.firstName} ${user.secondName} ${user.surName}`
    }
    return (
        <div className={classes.RatingRow}>
            <div>{getFullName()}</div>
            <div>{user.totalGames}</div>
            <div className={classes.Wins}>{user.wins}</div>
            <div className={classes.Looses}>{user.looses}</div>
            <div>{user.winRate}%</div>
        </div>
    )
}

export default RatingRow