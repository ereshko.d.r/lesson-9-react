import React from 'react'
import classes from './RatingContent.module.css'

const RatingContent = ({children, ...props}) => {
  return (
    <div className={classes.RatingContent}>
        <div className={classes.Table}>
            <div>ФИО</div>
            <div>Всего игр</div>
            <div>Победы</div>
            <div>Проигрыши</div>
            <div>Процент побед</div>
        </div>
        {children}
    </div>
  )
}

export default RatingContent