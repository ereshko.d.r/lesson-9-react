import React, { useEffect } from 'react'
import classes from '../style/RatingPage.module.css'
import MainWindow from '../../../UIs/windows/main-window/MainWindow'
import RatingContent from '../modules/rating-content/RatingContent'
import RatingRow from '../modules/rating-row/RatingRow'


const RatingPage = ({users, ...props}) => {
    const WINDOW_HEADER = 'Рейтинг игроков'

    const setTitle = useEffect(() => {
        document.title = WINDOW_HEADER
    }, [])
    return (
        <div className={classes.Main}>
            {setTitle}
            <div className={classes.Content}>
                <MainWindow header={WINDOW_HEADER}>
                    <RatingContent>
                        {users.sort((a, b) => b.winRate - a.winRate).map((user, index) => (
                            <RatingRow user={user} key={index}/>
                        ))}
                    </RatingContent>
                </MainWindow>
            </div>
        </div>
    )
}

export default RatingPage