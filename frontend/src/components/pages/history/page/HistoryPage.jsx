import React, { useEffect } from 'react'
import classes from '../style/HistoryPage.module.css'
import MainWindow from '../../../UIs/windows/main-window/MainWindow'
import HistoryContent from '../modules/history-content/HistoryContent'
import HistoryRow from '../modules/history-row/HistoryRow'

const HistoryPage = ({users, ...props}) => {
    const WINDOW_HEADER = 'История игр'

    const setTitle = useEffect(() => {
        document.title = WINDOW_HEADER
    }, [])
    return (
        <div className={classes.Main}>
            {setTitle}
            <div className={classes.Content}>
                <MainWindow header={WINDOW_HEADER}>
                    <HistoryContent>
                        {users.sort((a, b) => a.date - b.date).map((users, index) => (
                            <HistoryRow users={users} key={index} />
                        ))}
                    </HistoryContent>
                </MainWindow>
            </div>
        </div>
    )
}

export default HistoryPage