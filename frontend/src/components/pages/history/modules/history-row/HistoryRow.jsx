import React from 'react'
import Cross from '../../../../images/symbols/cross.svg'
import Zero from '../../../../images/symbols/zero.svg'
import Cup from '../../../../images/UI/small-cup.svg'
import classes from './HistoryRow.module.css'

const HistoryRow = ({users, ...props}) => {
    const getName = (player) => {
        return `${player.firstName} ${player.secondName[0]}. ${player.surName[0]}.`
    }
    return (
        <div className={classes.HistoryRow}>
            <div className={classes.PlayersContainer}>
                <div className={classes.PlayerField}>
                    <img src={Zero} alt="" width='16px'/>
                    {getName(users.player1)}
                    {(users.player1.isWin) ? <img src={Cup} alt='' width='24px'/> : null}
                </div>
                <div className={classes.Versus}>против</div>
                <div className={classes.PlayerField}>
                    <img src={Cross} alt="" width='16px'/>
                    {getName(users.player2)}
                    {(users.player2.isWin) ? <img src={Cup} alt='' width='24px'/> : null}
                </div>
            </div>
            <div className={classes.Date}>{users.date}</div>
            <div className={classes.Time}>{users.time}</div>
        </div>
    )
}

export default HistoryRow