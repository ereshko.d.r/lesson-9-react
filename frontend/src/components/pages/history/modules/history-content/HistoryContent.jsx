import React from 'react'
import classes from './HistoryContent.module.css'
import ThirdHeader from '../../../../UIs/textheaders/third-header/ThirdHeader'

const HistoryContent = ({children, ...props}) => {
  return (
        <div className={classes.HistoryContent}>
            <div className={classes.Table}>
                <ThirdHeader>Игроки</ThirdHeader>
                <ThirdHeader>Дата</ThirdHeader>
                <ThirdHeader>Время игры</ThirdHeader>
            </div>
            {children}
        </div>
    )
}

export default HistoryContent