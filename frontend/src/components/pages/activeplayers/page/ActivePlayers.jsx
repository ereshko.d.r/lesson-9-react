import React, { useState, useEffect } from 'react'
import classes from '../style/ActivePlayers.module.css'
import MainWindow from '../../../UIs/windows/main-window/MainWindow'
import SwitchToggle from '../../../UIs/switches/switch-toggle/SwitchToggle'
import ActivePlayersRow from '../modules/active-players-row/ActivePlayersRow'

const ActivePlayers = ({users, ...props}) => {
    const WINDOW_HEADER = 'Активные игроки'

    const setTitle = useEffect(() => {
        document.title = WINDOW_HEADER
    }, [])

    const [userList, setUserList] = useState(users)
    const [filter, setFilter] = useState(false)
    const useFilter = () => {
        if (filter) {
            setFilter(false)
            setUserList(users)
            console.log(userList)
        }
        else {
            setFilter(true)
            setUserList(users.filter(user => user.gameStatus === 'active'))
            console.log(userList)
        }
    }

    return (
        <div className={classes.Main}>
            {setTitle}
            <div className={classes.Content}>
                <MainWindow header={WINDOW_HEADER} headerItems={
                    <div className={classes.SwitchContainer}>
                        <div>Только свободные</div>
                        <SwitchToggle value={filter} onChange={useFilter}/>
                    </div>
                }>
                    {userList.map((user, index) => (
                        <ActivePlayersRow user={user} key={index}/>
                    ))}
                </MainWindow>

            </div>
        </div>
    )
}

export default ActivePlayers