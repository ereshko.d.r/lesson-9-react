import React from 'react'
import classes from './ActivePlayersRow.module.css'
import NormalStatusBar from '../../../../UIs/statusbars/normal-statusbar/NormalStatusBar'
import BackButton from '../../../../UIs/buttons/normal/back-button/BackButton'
import SubmitButton from '../../../../UIs/buttons/normal/submit-button/SubmitButton'

const ActivePlayersRow = ({user, ...props}) => {
    const status = user.gameStatus
    
    const getName = (player) => {
        return `${player.firstName} ${player.secondName} ${player.surName}`
    }

    const getStatusBar = (status) => {
        if (status === 'ingame') {
            return <NormalStatusBar color={'blue'}>В игре</NormalStatusBar>
        }
        return <NormalStatusBar color={'green'}>Свободен</NormalStatusBar>
    }

    const getButton = (status) => {
        if (status === 'ingame') {
            return <BackButton disabled>Позвать играть</BackButton>
        }
        return <SubmitButton>Позвать играть</SubmitButton>
    }

    return (
        <div className={classes.ActivePlayersRow}>
            <div>{getName(user)}</div>
            <div className={classes.ActivePlayersRowStatus}>
                <div className={classes.StatusBar}>{getStatusBar(status)}</div>
                <div>{getButton(status)}</div>
            </div>
        </div>
    )
}

export default ActivePlayersRow