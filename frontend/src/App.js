import React, { useState } from 'react'
import { BrowserRouter as Router, Routes, Route, Navigate} from 'react-router-dom';
import './App.css'
import users from './components/scripts/Users.json'
import subjects from './components/scripts/subjects.json'

import Header from './components/modules/header/Header';
import AuthPage from './components/pages/auth/page/AuthPage'
import GameFieldPage from './components/pages/gamefield/page/GameFieldPage';
import RatingPage from './components/pages/rating/page/RatingPage'
import ActivePlayersPage from './components/pages/activeplayers/page/ActivePlayers'
import HistoryPage from './components/pages/history/page/HistoryPage'
import UsersPage from './components/pages/users/page/UsersPage'


function App() {
    const [userList, setUserList] = useState(users.User)

    return (
        <Router>
            <Routes>
                <Route path='/' element={<Navigate to={'/auth'}/>}/>
                <Route path={'/auth'} element={<AuthPage />} />
                <Route element={<Header />}>
                    <Route path={'/game'} element={<GameFieldPage users={subjects}/>}/>
                    <Route path={'/rating'} element={<RatingPage users={userList}/>} setUserList={setUserList}/>
                    <Route path={'/active'} element={<ActivePlayersPage users={userList} setUserList={setUserList}/>}/>
                    <Route path={'/history'} element={<HistoryPage users={users.History}/>}/>
                    <Route path={'/users'} element={<UsersPage users={userList} setUserList={setUserList}/>} />
                </Route>
            </Routes>
        </Router>
        
    );
}

export default App;
